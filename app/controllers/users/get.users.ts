import {AuthenticatedRequest, OauthBearerCredentials, Request, Response} from "alpha-omega";
import {User} from "../../models/user";
import {DataOutputOrderType} from "alpha-omega/dist/orm/common/model";

export class GetUsersController {
	
	/**
	 * Get one user by ID
	 *
	 * @param req
	 * @param res
	 */
	public static async getOne(req: Request, res: Response) {
		
		const userId = Number(req.params.id);
		
		const user = await User.find(userId);
		
		res.json(user.toObject());
	
	}
	
	/**
	 * Get one user by FID
	 *
	 * @param req
	 * @param res
	 */
	public static async getByFid(req: Request, res: Response) {
		
		const fid = String(req.params.id);
		
		const user = await User.getUserByFID(fid);
		
		if(user !== null) {
			return res.json(user.toObject());
		}
		
		res.status(404).send();
		
	}
	
	/**
	 * Get all of the User friends
	 *
	 * @param req
	 * @param res
	 */
	public static async getList(req: Request, res: Response) {
		
		const page = !isNaN(Number(req.query.page)) ? Number(req.query.page) : 1;
		const items = !isNaN(Number(req.query.items)) ? Number(req.query.items) : 10;
		
		const users = await User.list<User>(page, items, [
				{
					name: "id",
					order: DataOutputOrderType.DESC
				}
			]
		);
	
		const out = new Array(users.length);
		
		for(let i = 0; i < users.length; i++) {
			out[i] = users[i].toObject();
		}
		
		res.json(out);
		
	}
	
	/**
	 * Get all of the User friends
	 *
	 * @param req
	 * @param res
	 */
	public static async getFriends(req: Request, res: Response) {
		
		const page = !isNaN(Number(req.query.page)) ? Number(req.query.page) : 1;
		const items = !isNaN(Number(req.query.items)) ? Number(req.query.items) : 10;
		
		const userId = Number(req.params.id);
		
		const user = await User.find(userId);
		
		const friends = await user.paginate(items, page).friendsWith;
		
		const out = new Array(friends.length);
		
		for(let i = 0; i < friends.length; i++) {
			out[i] = friends[i].toObject();
		}
		
		res.json(out);
		
	}
	
	/**
	 * Get all of the User friends
	 *
	 * @param req
	 * @param res
	 */
	public static async getMyFriends(req: AuthenticatedRequest<OauthBearerCredentials>,
	                               res: Response) {
		
		const page = !isNaN(Number(req.query.page)) ? Number(req.query.page) : 1;
		const items = !isNaN(Number(req.query.items)) ? Number(req.query.items) : 10;
		
		const userId = Number(req.auth.data.user);
		
		const user = await User.find(userId);
		
		const friends = await user.paginate(items, page).friendsWith;
		
		const out = new Array(friends.length);
		
		for(let i = 0; i < friends.length; i++) {
			out[i] = friends[i].toObject();
		}
		
		res.json(out);
		
	}
	
	/**
	 * Get list of current dictators
	 *
	 * @param req
	 * @param res
	 */
	public static async getDictatorList(req: Request, res: Response) {
	
		const users = await User.list<User>(
			1,
			10,
			[
				{
					name: "dinesGathered",
					order: DataOutputOrderType.DESC
				}]
		);
		
		const out = new Array(users.length);
		
		for(let i = 0; i < users.length; i++) {
			out[i] = users[i].toObject();
		}
		
		res.json(out);
	
	}
	
}