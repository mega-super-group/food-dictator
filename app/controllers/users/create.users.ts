import {Request, Response} from "alpha-omega";
import {User} from "../../models/user";
import {FacebookMeHelper} from "../../external-api/facebook-me/facebook.me.helper";
import {AccessKey} from "../../models/access.key";
import {DataOutputOrderType} from "alpha-omega/dist/orm/common/model";
import {SeedUsers} from "../seed/seed.users";

export class CreateUsersController {
	
	/**
	 * Create User from simple JSON information
	 *
	 * @param req
	 * @param res
	 */
	public static async create(req: Request, res: Response) {
		
		if(req.json) {
		
			const userDef = req.json as User;
			
			const user = new User(userDef);
		
			const dup = await User.getUserByFID(user.fid);
			
			/*
				Preemptive return of existing user for that id
			 */
			if(dup !== null) {
				
				const key = await AccessKey.createForUser(dup);
				
				await dup
					.paginate(1)
					.orderBy([
						{
							name: "createdAt",
							order: DataOutputOrderType.DESC
						}])
					.accessKeys;
				
				return res.json(dup.toObject());
				
			}
			
			await AccessKey.createForUser(dup);
			await user.paginate(1).orderBy([
				{
					name: "createdAt",
					order: DataOutputOrderType.DESC
				}]
			).accessKeys;
			
			return res.json(user.toObject());
			
		}
	
	}
	
	/**
	 * Create User from Facebook fetched information
	 *
	 * @param req
	 * @param res
	 */
	public static async createFIDUser(req: Request, res: Response) {
		
		const key: string = req.params.key;
		
		const fbConn = new FacebookMeHelper(key);
		
		try {
			
			const userData = await fbConn.fetchUserInformation();
			
			const dup = await User.getUserByFID(userData.id);
			
			if(dup !== null) {
				
				const key = await AccessKey.createForUser(dup);
				
				await dup
					.paginate(1)
					.orderBy([
						{
							name: "createdAt",
							order: DataOutputOrderType.DESC
						}])
					.accessKeys;
				
				return res.json(dup.toObject());
				
			}
			
			const user = new User();
			user.fid = userData.id;
			user.first = userData.first_name;
			user.last = userData.last_name;
			user.name = userData.name;
			user.email = userData.email;
			
			await user.save();
			
			await AccessKey.createForUser(user);
			await user.paginate(1).orderBy([
				{
					name: "createdAt",
					order: DataOutputOrderType.DESC
				}]
			).accessKeys;
			
			SeedUsers.seedUserFriends(user);
			
			return res.json(user.toObject());
			
		} catch (e) {
			
			return res.status(e.code);
			
		}
		
	}
	
}