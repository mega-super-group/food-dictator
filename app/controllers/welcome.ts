import {Request, Response} from "alpha-omega";

export class Welcome {

    public static async welcomeMessage(request: Request, response: Response) {

        response.status(200).send("Welcome to Alpha Omega!");

    }

}