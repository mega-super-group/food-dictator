import {Request, Response} from "alpha-omega";
import {GooglePlacesHelper} from "../../external-api/google-places/google.places.helper";
import {Place as GPlace} from "../../external-api/google-places/google.places.interface";
import {Place} from "../../models/place";
import {DataOutputOrderType} from "alpha-omega/dist/orm/common/model";

export class GetPlacesController {
	
	/**
	 * Retrieve place by ID
	 *
	 * @param req
	 * @param res
	 */
	public static async getOne(req: Request, res: Response) {
		
		const userId = Number(req.params.id);
		
		const place = await Place.find(userId);
		
		res.json(place.toObject());
		
	}
	
	/**
	 * Retrieve places paginated
	 *
	 * @param req
	 * @param res
	 */
	public static async getList(req: Request, res: Response) {
		
		const page = !isNaN(Number(req.query.page)) ? Number(req.query.page) : 1;
		const items = !isNaN(Number(req.query.items)) ? Number(req.query.items) : 10;
		
		const places = await Place.list<Place>(page, items, [
				{
					name:"createdAt",
					order: DataOutputOrderType.DESC
				}
			]
		);
		
		const out = new Array(places.length);
		
		for(let i = 0; i < places.length; i++) {
			out[i] = places[i].toObject();
		}
		
		res.json(out);
		
	}
	
	/**
	 * Retrieve places for some location
	 *
	 * @param req
	 * @param res
	 */
	public static async getForLocation(req: Request, res: Response) {
		
		const lat = String(req.query.lat);
		const lon = String(req.query.lon);
		const placeType = String(req.query.type).toLowerCase();
		const keyword = String(req.query.keyword);
		
		let type = GooglePlacesHelper.validatePlaceType(placeType);
		
		/*
		 *  Request list of places for some location from Google
		 */
		const gPlaces = await GooglePlacesHelper.getPlacesForLocationRequest({
			lat,
			lon,
			type,
			keyword
		});
		
		/*
		 *  Index google places for fast search and compare
		 */
		const gidMap: Map<string, GPlace> = new Map<string, GPlace>();
		
		for(const place of gPlaces.results) {
			gidMap.set(place.id, place);
		}
		
		const keyList = [...gidMap.keys()];
		
		/*
		 *  Request list of places from system
		 */
		const places = await Place.getListForGIDList(keyList);
		
		/*
		 *  Index system places for fast search and compare
		 */
		const placeMap: Map<string, Place> = new Map<string, Place>();
		
		for(const place of places) {
			placeMap.set(place.googleId, place);
		}
		
		/*
		 *  Start synchronization and output
		 */
		const output: Object[] = [];
		
		for(const key of keyList) {
			
			if(!placeMap.has(key)) {
				
				const gPlace = gidMap.get(key);
				const place = new Place();
				place.googleId = gPlace.id;
				place.address = gPlace.vicinity;
				place.name = gPlace.name;
				place.rating = gPlace.rating;
				
				await place.save();
				output.push(place.toObject());
				
			} else {
				
				const gPlace = gidMap.get(key);
				const place = placeMap.get(key);
				place.rating = gPlace.rating;
				place.address = gPlace.vicinity;
				place.name = gPlace.name;
				
				await place.save();
				output.push(place.toObject());
				
			}
			
		}
		
		res.json(output);
		
	}
	
}