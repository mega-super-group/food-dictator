import {AccessKey} from "../../models/access.key";
import {User} from "../../models/user";
import {Dine} from "../../models/dine";
import {Place} from "../../models/place";
import {Invite} from "../../models/invite";
import {App} from "alpha-omega";

export class SystemCommands {
	
	public static async truncateAll() {
		
		await AccessKey.truncate();
		await User.truncate();
		await Dine.truncate();
		await Place.truncate();
		await Invite.truncate();
		
		App.log("Truncation end", "SYSTEM:TRUNCATE");
		
	}
	
}