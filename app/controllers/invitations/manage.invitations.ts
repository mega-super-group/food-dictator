import {AuthenticatedRequest, OauthBearerCredentials, Request, Response} from "alpha-omega";
import {User} from "../../models/user";
import {InvitationStatus, Invite} from "../../models/invite";

export class ManageInvitationsController {
	
	public static async listForUserSent(req: AuthenticatedRequest<OauthBearerCredentials>,
	                                    res: Response) {
	
		const userId = Number(req.auth.data.user);
	
		const user = await User.find<User>(userId);
		
		const inv = await user.sentInvites;
		
		const out = new Array(inv.length);
		
		for(let i = 0; i < inv.length; i++) {
			out[i] = inv[i].toObject();
		}
		
		return res.json(out);
		
	}
	
	public static async listForUserReceived(req: AuthenticatedRequest<OauthBearerCredentials>,
	                                        res: Response) {
		
		const userId = Number(req.auth.data.user);
		
		const user = await User.find<User>(userId);
		
		const inv = await user.gotInvites;
		
		const out = new Array(inv.length);
		
		for(let i = 0; i < inv.length; i++) {
			out[i] = inv[i].toObject();
		}
		
		return res.json(out);
		
	}
	
	public static async create(req: AuthenticatedRequest<OauthBearerCredentials>, res: Response) {
		
		const userId = Number(req.auth.data.user);
		
		return res.send();
		
	}
	
	public static async accept(req: AuthenticatedRequest<OauthBearerCredentials>, res: Response) {
		
		const invId = Number(req.params.id);
		
		const invite = await Invite.find(invId);
		
		const userId = Number(req.auth.data.user);
		
		const user = await User.find<User>(userId);
		
		// Just for reasons of testing by one user
		await invite.host;
		await invite.invitee;
		
		// Checkup for host and invitee is jsut for testing
		if(invite.host.id === user.id || invite.invitee.id === user.id) {
			invite.status = InvitationStatus.ACCEPTED;
			await invite.save();
			return res.json(invite.toObject());
		}
		
		return res.status(403);
		
	}
	
	public static async tentative(req: AuthenticatedRequest<OauthBearerCredentials>, res: Response) {
		
		const invId = Number(req.params.id);
		
		const invite = await Invite.find(invId);
		
		const userId = Number(req.auth.data.user);
		
		const user = await User.find<User>(userId);
		
		// Just for reasons of testing by one user
		await invite.host;
		await invite.invitee;
		
		// Checkup for host and invitee is jsut for testing
		if(invite.host.id === user.id || invite.invitee.id === user.id) {
			invite.status = InvitationStatus.TENTATIVE;
			await invite.save();
			return res.json(invite.toObject());
		}
		
		return res.status(403);
		
	}
	
	public static async reject(req: AuthenticatedRequest<OauthBearerCredentials>, res: Response) {
		
		const invId = Number(req.params.id);
		
		const invite = await Invite.find(invId);
		
		const userId = Number(req.auth.data.user);
		
		const user = await User.find<User>(userId);
		
		// Just for reasons of testing by one user
		await invite.host;
		await invite.invitee;
		
		// Checkup for host and invitee is jsut for testing
		if(invite.host.id === user.id || invite.invitee.id === user.id) {
			invite.status = InvitationStatus.REJECTED;
			await invite.save();
			return res.json(invite.toObject());
		}
		
		return res.status(403);
		
	}
	
}