import {AuthenticatedRequest, OauthBearerCredentials, Request, Response} from "alpha-omega";
import {User} from "../../models/user";
import {Dine, DineStatus} from "../../models/dine";

export class GetDinesController {
	
	public static async getOne(req: AuthenticatedRequest<OauthBearerCredentials>,
	                           res: Response) {
		
		const id = Number(req.params.id);
		
		const uid = Number(req.auth.data.user);
		
		const user = await User.find<User>(uid);
		
		const dine = await Dine.find<Dine>(id);
		
		const host = await dine.host;
		const guests = await dine.guests;
		await dine.place;
		await dine.server;
		
		if(host.id === user.id)
			return res.json(dine.toObject());
		
		const userFound = guests.filter(g =>  g.id === user.id);
		
		if(userFound.length === 1)
			return res.json(dine.toObject());
		
		return res.status(404).send("No matches found");
		
	}
	
	public static async getAwaitingForUser(req: AuthenticatedRequest<OauthBearerCredentials>,
	                                       res: Response) {
		
		const id = Number(req.auth.data.user);
		
		const user = await User.find<User>(id);
		
		const dines = await user.seek()
			.where("status").equals(DineStatus.PENDING)
			.among().dines;
		
		const dinesHost = await user.seek()
			.where("status").equals(DineStatus.PENDING)
			.among().hostingDine;
		
		const out = new Array(dines.length + dinesHost.length);
		
		let k = 0;
		
		for(let i = 0; i < dinesHost.length; i++) {
			out[k++] = dinesHost[i].toObject();
		}
		
		for(let i = 0; i < dines.length; i++) {
			out[k++] = dines[i].toObject();
		}
		
		res.json(out);
		
	}
	
	public static async getPastForUser(req: AuthenticatedRequest<OauthBearerCredentials>,
	                                   res: Response) {
		
		const id = Number(req.auth.data.user);
		
		const user = await User.find<User>(id);
		
		const dines = await user.seek()
			.where("status").equals(DineStatus.CLOSED)
			.among().dines;
		
		const dinesHost = await user.seek()
			.where("status").equals(DineStatus.CLOSED)
			.among().hostingDine;
		
		const out = new Array(dines.length + dinesHost.length);
		
		let k = 0;
		
		for(let i = 0; i < dinesHost.length; i++) {
			out[k++] = dinesHost[i].toObject();
		}
		
		for(let i = 0; i < dines.length; i++) {
			out[k++] = dines[i].toObject();
		}
		
		res.json(out);
		
	}
	
	
	
}