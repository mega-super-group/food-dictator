import {AuthenticatedRequest, OauthBearerCredentials, Request, Response} from "alpha-omega";
import {Dine, DineStatus} from "../../models/dine";
import {User} from "../../models/user";

export class UpdateDineController {
	
	public static async closeDine(req: AuthenticatedRequest<OauthBearerCredentials>,
	                              res: Response) {
		
		const userId = Number(req.auth.data.user);
		
		const user = await User.find<User>(userId);
		
		const dineId = Number(req.params.id);
		
		const dine = await Dine.find<Dine>(dineId);
		
		const host = await dine.host;
		
		if(host.id !== user.id) {
			return res.status(403).send("Insufficient rights");
		}
		
		const guests = await dine.guests;
		const place = await dine.place;
		
		host.dinesGathered++;
		await host.save();
		
		for(const g of guests) {
			g.dinesBeen++;
			await g.save();
		}
		
		if(place !== null) {
			place.visits++;
			await place.save();
		}
		
		dine.status = DineStatus.CLOSED;
		await dine.save();
		
		await dine.host;
		await dine.place;
		await dine.guests;
		
		res.json(dine.toObject());
		
	}
	
	public static async acceptDine(req: Request, res: Response) {
		
		const dineId = Number(req.params.id);
	
	}
	
}