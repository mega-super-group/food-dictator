import {App, AuthenticatedRequest, OauthBearerCredentials, Request, Response} from "alpha-omega";
import {Dine} from "../../models/dine";
import {User} from "../../models/user";
import {Place} from "../../models/place";
import {Invite} from "../../models/invite";

export class CreateDineController {
	
	/**
	 * Create Dine
	 *
	 * @param req
	 * @param res
	 */
	public static async create(req: AuthenticatedRequest<OauthBearerCredentials>,
	                           res: Response) {
		
		const uid = Number(req.auth.data.user);
		
		const user = await User.find<User>(uid);
		
		if(req.json) {
		
			const reqDine = req.json as Dine;
			
			const dine = new Dine();
			
			dine.host = user;
			dine.title = reqDine.title;
			dine.message = reqDine.message;
			
			const guests: User[] = [];
			
			for (const guest of reqDine.guests) {
				
				try {
					
					const realGuest = await User.find<User>(guest.id);
					
					guests.push(realGuest);
					
				} catch (e) {
				
					App.error("Dine can't be populated for user with id: " + guest.id, "DINE:CREATE");
					
				}
				
			}
			
			dine.guests = guests;
			
			try {
				
				const place = await Place.find<Place>(Number(reqDine.place.id));
				
				dine.place = place;
				
				await dine.save();
				
				place.dines = [dine];
				
				await place.save();
				
				CreateDineController.propagateInvites(dine);
				
				res.json(dine.toObject());
				
			} catch (e) {
				
				res.status(e.code).send("Such place not found");
				
			}
			
		}
	
	}
	
	/**
	 * Create invites for every Guest of Dine
	 *
	 * @param dine
	 */
	private static async propagateInvites(dine: Dine) {
	
		const guests = await dine.guests;
		const place = await dine.place;
		const host = await dine.host;
		
		for(const g of guests) {
			
			const invite = new Invite();
			invite.dine = dine;
			invite.place = place;
			invite.host = host;
			invite.invitee = g;
			await invite.save();
			
		}
	
	}
	
}