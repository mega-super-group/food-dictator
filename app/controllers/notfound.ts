import {Request, Response} from "alpha-omega";
import {App} from "alpha-omega";

export class NotFoundController {

    public static handle(req: Request, res: Response): Response {

        App.log("404 NOT_FOUND " + req.method + ' ' + (req.url.toString()) + ' ' + (new Date().toISOString()), "app::router");

        return res.status(404).send("Not found");

    }

}