import {BelongsToMany, BelongsToOne, HasMany, MongoModel, Property} from "alpha-omega/dist/orm/mongo.model";
import {Type} from "alpha-omega";
import {User} from "./user";
import {Place} from "./place";
import {Invite} from "./invite";

export class Dine extends MongoModel {
	
	@Property({primary: true, auto: true, type: Type.Int})
	public id: number = null;
	
	@Property()
	public title: string = "";
	
	@Property()
	public status: DineStatus = DineStatus.PENDING;
	
	@Property()
	public message: string = "";
	
	@Property()
	public total: number = 0;
	
	@BelongsToOne(() => User, {weak: true})
	public server: User = null;
	
	@BelongsToOne(() => User)
	public host: User = null;
	
	@BelongsToMany(() => User)
	public guests: User[] = [];
	
	@BelongsToOne(() => Place)
	public place: Place = null;
	
	@HasMany(() => Invite)
	public invite: Invite[] = [];
	
}

export enum DineStatus {
	PENDING,
	PROCESSING,
	CLOSED
}