import {BelongsToMany, HasMany, MongoModel, Property} from "alpha-omega/dist/orm/mongo.model";
import {Type} from "alpha-omega";
import {Dine} from "./dine";
import {Invite} from "./invite";
import {AccessKey} from "./access.key";

export class User extends MongoModel {
	
	@Property({primary: true, auto: true, type: Type.Int})
	public id: number = null;
	
	@Property({unique: true, expose: false})
	public fid: string = null;
	
	@Property({index: true})
	public email: string = null;
	
	@Property({index: true})
	public name: string = null;
	
	@Property()
	public first: string = null;
	
	@Property()
	public last: string = null;
	
	@Property()
	public dinesGathered: number = 0;
	
	@Property()
	public dinesBeen: number = 0;
	
	/*
	 *  Relations
	 */
	@HasMany(()=>User)
	public friendsWith: User[] = [];
	
	@BelongsToMany(()=>User, {weak: true})
	public friendedBy: User[] = [];
	
	@HasMany(()=>Dine)
	public servingDine: Dine[] = [];
	
	@HasMany(()=>Dine)
	public hostingDine: Dine[] = [];
	
	@HasMany(()=>Dine)
	public dines: Dine[] = [];
	
	@HasMany(()=>Invite)
	public sentInvites: Invite[] = [];
	
	@HasMany(()=>Invite)
	public gotInvites: Invite[] = [];
	
	@HasMany(()=>AccessKey)
	public accessKeys: AccessKey[] = [];
	
	public static getUserByFID(fid: string) : Promise<User> {
		
		return User.query<User>().construct().where("fid").equals(fid).getOne();
		
	}
	
}