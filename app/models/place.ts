import {HasMany, MongoModel, Property} from "alpha-omega/dist/orm/mongo.model";
import {Type} from "alpha-omega";
import {Dine} from "./dine";
import {Invite} from "./invite";

export class Place extends MongoModel {
	
	@Property({primary: true, auto: true, type: Type.Int})
	public id: number = null;
	
	@Property({unique: true, expose: false})
	public googleId: string = null;
	
	@Property({index: true})
	public name: string = null;
	
	@Property({index: true})
	public rating: number = 0;
	
	@Property({index: true})
	public visits: number = 0;
	
	@Property()
	public address: string = "";
	
	@Property()
	public createdAt: Date = new Date();
	
	@HasMany(()=>Dine)
	public dines: Dine[] = [];
	
	@HasMany(()=>Invite)
	public invites: Invite[] = [];
	
	public static async getListForGIDList(list: string[]) : Promise<Place[]> {
		
		return Place.query<Place>()
			.construct()
			.where("googleId")
			.in(list)
			.getMany();
		
	}
	
}