import {BelongsToOne, MongoModel, Property} from "alpha-omega/dist/orm/mongo.model";
import {Type} from "alpha-omega";
import {User} from "./user";
import {Place} from "./place";
import {Dine} from "./dine";

export class Invite extends MongoModel {
	
	@Property({primary: true, auto: true, type: Type.Int})
	public id: number = null;
	
	@Property({index: true})
	public status: InvitationStatus = InvitationStatus.PENDING;
	
	@BelongsToOne(()=>User)
	public host: User = null;
	
	@BelongsToOne(() => User)
	public invitee: User = null;
	
	@BelongsToOne(() => Place)
	public place: Place = null;
	
	@BelongsToOne(() => Dine)
	public dine: Dine = null;
	
}

export enum InvitationStatus {
	PENDING,
	ACCEPTED,
	TENTATIVE,
	REJECTED,
	CLOSED,
	EXPIRED
}