import {App} from "alpha-omega";
import {GooglePlacesHelper} from "./external-api/google-places/google.places.helper";

App.init("dist/routes").then(() => {

	GooglePlacesHelper.init(App.config.get("api")["google-places"]);
	
	App.start(8080);

});

