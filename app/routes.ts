import {App} from "alpha-omega";
import {ParseRequest} from "./middleware/parse.request";
import {AfterRequest} from "./middleware/after.request";
import {NotFoundController} from "./controllers/notfound";
import {ErrorController} from "./controllers/error";
import {BeforeRequest} from "./middleware/before.request";
import {GetPlacesController} from "./controllers/places/get.places";
import {GetUsersController} from "./controllers/users/get.users";
import {CreateUsersController} from "./controllers/users/create.users";
import {CreateDineController} from "./controllers/dining/create.dine";
import {GetDinesController} from "./controllers/dining/get.dines";
import {AuthRequest} from "./middleware/auth.request";
import {ManageInvitationsController} from "./controllers/invitations/manage.invitations";
import {ManageAuthorization} from "./controllers/auth/manage.authorization";
import {SystemCommands} from "./controllers/cmd/system.commands";
import {UpdateDineController} from "./controllers/dining/update.dine";

export function routeStack() {

    App.router.handleBefore(ParseRequest.handle);
    App.router.handleBefore(BeforeRequest.handle);
    App.router.handleAfter(AfterRequest.handle);
    App.router.handleNotFound(NotFoundController.handle);
    App.router.handleError(ErrorController.handle);

    const auth = AuthRequest.handle;
    
    /* *******************************************************************************
     *
     *                                  API CALLS
     *
     * *******************************************************************************
     */
    
    App.router.get("/api/v1/authorized", ManageAuthorization.checkAuthorized, [auth]);
    
    App.router.get("/api/v1/places/search/location", GetPlacesController.getForLocation);
	App.router.get("/api/v1/places", GetPlacesController.getList);
	App.router.get("/api/v1/place/:id", GetPlacesController.getOne);
	
	App.router.get("/api/v1/users", GetUsersController.getList);
	App.router.get("/api/v1/user/:id", GetUsersController.getOne);
	App.router.get("/api/v1/user/friends/:id", GetUsersController.getFriends);
	App.router.get("/api/v1/user/fid/:id", GetUsersController.getByFid);
	App.router.post("/api/v1/user", CreateUsersController.create);
	App.router.post("/api/v1/user/fid/:key", CreateUsersController.createFIDUser);
	
	App.router.post("/api/v1/dine", CreateDineController.create, [auth]);
	App.router.get("/api/v1/dines/:id", GetDinesController.getOne, [auth]);
	App.router.put("/api/v1/dine/:id/close", UpdateDineController.closeDine, [auth]);
	
	App.router.get("/api/v1/me/dines/active", GetDinesController.getAwaitingForUser, [auth]);
	App.router.get("/api/v1/me/dines/past", GetDinesController.getPastForUser, [auth]);
	App.router.get("/api/v1/me/invites/sent", ManageInvitationsController.listForUserSent, [auth]);
	App.router.get("/api/v1/me/invites/received", ManageInvitationsController.listForUserReceived, [auth]);
	App.router.post("/api/v1/me/invite", ManageInvitationsController.create, [auth]);
	App.router.put("/api/v1/me/invite/:id/accept", ManageInvitationsController.accept, [auth]);
	App.router.put("/api/v1/me/invite/:id/tentative", ManageInvitationsController.tentative, [auth]);
	App.router.put("/api/v1/me/invite/:id/reject", ManageInvitationsController.reject, [auth]);
	App.router.get("/api/v1/me/friends", GetUsersController.getMyFriends, [auth]);
	
	App.router.get("/api/v1/dictators", GetUsersController.getDictatorList);
	
	App.router.command("truncate-all", SystemCommands.truncateAll);
	
}