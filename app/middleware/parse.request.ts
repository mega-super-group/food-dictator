import {App} from "alpha-omega";
import {Request, Response} from "alpha-omega";

export class ParseRequest {

    /**
     * Handle Request data
     * ----------------------------------------------------------
     * Data will be transformed to some specific data type
     * according to parameters defined for system
     *
     * @param {Request} req
     * @param {Response} res
     * @param next
     */
    public static handle(req: Request, res: Response, next: any) {

        let body: string = "";

        // Make Request JSON field defaulted to null
        req.json = null;

        // Receive data
        req.on("data", part => body += part);

        // Define request body and expected JSON input if any came
        req.on("end", () => {

            req.body = body;

            // Most of our communications is chunks of JSON data
            // if we have some content with positive length then
            // system should try to convert it to JSON object
            if(body.length > 0) {

                // If some exemplar of system is more complicated than adjusted to use
                // JSON only type of data, please add here check for header type and
                // create specific request container property for specific data type
                try {

                    req.json = JSON.parse(body);

                } catch (e) {

                    App.log("Data came with Request is not JSON compatible", "ROUTER::PARSER");

                }

            }

            // Call next callable executor in the pipeline
            next();

        });

    }

}