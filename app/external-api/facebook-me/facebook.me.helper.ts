import {Connector} from "driver-connector";
import {Exception} from "ts-exceptions";
import {FacebookMeInterface} from "./facebook.me.interface";

export class FacebookMeHelper {
	
	private static connector: Connector = null;
	private key: string = null;
	
	constructor(key: string) {
		this.key = key;
		
		if(FacebookMeHelper.connector === null) {
			FacebookMeHelper.init();
		}
	}
	
	/**
	 * Initialize Helper with some key
	 * -------------------------------------------------------------
	 */
	public static init() {
		this.connector = new Connector();
		this.connector._conf.setURI("https://graph.facebook.com");
	}
	
	/**
	 * Get all places of certain type
	 * -------------------------------------------------------------
	 * @param request
	 */
	public async fetchUserInformation() : Promise<FacebookMeInterface> {
		
		let query = FacebookMeHelper.createMeQuery(this.key);
		
		const res = await FacebookMeHelper
			.connector
			.get("/v3.1/me?" + query, {});
		
		if(res.status < 300) {
			return res.data as FacebookMeInterface;
		}
		
		throw new Exception(
			"Cannot retrieve information: " + res.error, res.status);
		
	}
	
	/**
	 * Method to transform request interface
	 * -------------------------------------------------------------
	 * @param key
	 */
	private static createMeQuery(key: string) : string {
		
		let query = "access_token=" + key;
		query += "&fields=email,first_name,gender,last_name,name";
		
		return query;
		
	}
	
}