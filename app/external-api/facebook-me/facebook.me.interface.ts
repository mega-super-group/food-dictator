export interface FacebookMeInterface {
	email: string;
	first_name: string;
	last_name: string;
	name: string;
	id: string;
}