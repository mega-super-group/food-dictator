import {PlaceType} from "./google.places.helper";

export interface LocationRequest {
	lat: string;
	lon: string;
	type: PlaceType;
	keyword: string;
	opennow?: boolean;
}

export interface PlaceList {
	html_atributions: any[];
	results: Place[];
}

export interface Place {
	geometry: any;
	icon: string;
	id: string;
	name: string;
	opening_hours: PlaceOpeningHours;
	photos: PlacePhoto[];
	place_id: string;
	price_level: number;
	rating: number;
	types: string[];
	vicinity: string;
}

export interface PlacePhoto {
	height: number;
	width: number;
	html_atributions: any[];
	photo_reference: string;
}

export interface PlaceOpeningHours {
	open_now: boolean;
}