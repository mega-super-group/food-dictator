import {Connector} from "driver-connector";
import {Exception} from "ts-exceptions";
import {LocationRequest, PlaceList} from "./google.places.interface";

export class GooglePlacesHelper {
	
	private static connector: Connector = null;
	private static key: string = null;
	
	/**
	 * Initialize Helper with some key
	 * -------------------------------------------------------------
	 * @param key
	 */
	public static init(key: string) {
		
		this.connector = new Connector();
		this.connector._conf.setURI("https://maps.googleapis.com");
		this.key = key;
		
	}
	
	/**
	 * Get all places of certain type
	 * -------------------------------------------------------------
	 * @param request
	 */
	public static async getPlacesForLocationRequest(request: LocationRequest) : Promise<PlaceList> {
		
		let query = GooglePlacesHelper.createLocationQueryFromRequest(request);
		
		const res = await GooglePlacesHelper
			.connector
			.get("/maps/api/place/nearbysearch/json?" + query, {});

		if(res.status < 300) {
			return res.data as PlaceList;
		}
		
		throw new Exception(
			"Cannot retrieve information: " + res.error, res.status);
		
	}
	
	/**
	 * Validate type of some place
	 * -------------------------------------------------------------
	 * @param place
	 */
	public static validatePlaceType(place: string) : PlaceType {
		
		place = place.toLowerCase();
		let type: PlaceType = PlaceType.RESTAURANT;
		
		for(const key in PlaceType) {
			
			if(key.toLowerCase() === place) {
				type = PlaceType[key] as any;
			}
			
		}
		
		return type;
	
	}
	
	/**
	 * Method to transform request interface
	 * -------------------------------------------------------------
	 * @param request
	 */
	private static createLocationQueryFromRequest(request: LocationRequest) : string {
		
		const r = request as any;
		const type = PlaceType[request.type].toLowerCase();
		
		let query = "key="+GooglePlacesHelper.key;
		let location = new Array(2);
		
		for(const k in r) {
			
			if(k === "lat") {
				location[0] = r[k];
			}
			else if(k === "lon") {
				location[1] = r[k];
			}
			else if(k === "opennow") {
				query += '&' + k;
			}
			else if(k === "type") {
				query += '&' + k + '=' + type;
			}
			else {
				query += '&' + k + '=' + r[k];
			}
			
		}
		
		query += "&location=" + location[0] + ',' + location[1];
		query += "&radius=1500";
		query += "&fields=photos,formatted_address,name,rating,opening_hours";
		
		return query;
		
	}
	
}

export enum PlaceType {
	RESTAURANT,
	CAFE,
	DINER,
	STEAKHOUSE,
	BRUNCH,
	BREAKFAST,
	LUNCH,
	DINNER,
	BAR,
	PUB,
}