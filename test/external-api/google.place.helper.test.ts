import {expect} from "chai";
import {GooglePlacesHelper, PlaceType} from "../../app/external-api/google-places/google.places.helper";
import {LocationRequest} from "../../app/external-api/google-places/google.places.interface";

describe("Google places helper test", () => {
	
	/* -----------------------------------------------------------------
	 *                       Prepare environment
	 * -----------------------------------------------------------------*/
	
	before(() => {
		
		return new Promise((resolve => {
			GooglePlacesHelper.init("ABRACADABRA");
			resolve();
		}));
		
	});
	
	/* -----------------------------------------------------------------
	 *                          Prepare tests
	 * -----------------------------------------------------------------*/
	
	beforeEach(() => {
	
	
	
	});
	
	afterEach(() => {
	

	
	});
	
	/* -----------------------------------------------------------------
	 *                              Run tests
	 * -----------------------------------------------------------------*/
	
	it("Should validate restaurant type", () => {
		
		expect(GooglePlacesHelper.validatePlaceType("restaurant"))
			.to.equals(PlaceType.RESTAURANT, "Should be restaurant when restaurant passed");
		expect(GooglePlacesHelper.validatePlaceType("abracadabra"))
			.to.equals(PlaceType.RESTAURANT, "Should be defaulted to restaurant on non matching input");
		expect(GooglePlacesHelper.validatePlaceType("brunch"))
			.to.equals(PlaceType.BRUNCH, "Should produce Brunch");
		expect(GooglePlacesHelper.validatePlaceType("breakfast"))
			.to.equals(PlaceType.BREAKFAST, "Should produce breakfast");
		
	});
	
	it("Should make proper Location Query", () => {
		
		const pvt = GooglePlacesHelper as any;
		const key = pvt.key;
		
		const req1: LocationRequest = {
			lat: "123.456",
			lon: "456.123",
			type: PlaceType.CAFE,
			keyword: "sushi",
			opennow: true
		};
		
		expect(pvt.createLocationQueryFromRequest(req1))
			.to
			.equals("key="+key+"&type=cafe&keyword=sushi&opennow&location=123.456,456.123");
		
	});
	
	
});